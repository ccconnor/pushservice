module pushservice

go 1.14

require (
	github.com/go-redis/redis v6.15.7+incompatible
	github.com/gorilla/websocket v1.4.2
	github.com/jinzhu/gorm v1.9.12
	github.com/pkg/errors v0.9.1 // indirect
	github.com/shopspring/decimal v1.2.0
	github.com/spf13/viper v1.7.0
	pack.ag/amqp v0.12.5
)
