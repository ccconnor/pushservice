package common

import (
	"github.com/spf13/viper"
	"log"
	"os"
	"path/filepath"
)

var Config *config

type config struct {
	Addr  string      `yaml:"addr"`
	Port  int         `yaml:"port"`
	Mysql mysqlConfig `yaml:"mysql"`
	Redis redisConfig `yaml:"redis"`
	Amqp  amqpConfig  `yaml:"amqp"`
}

type mysqlConfig struct {
	Host     string `yaml:"host"`
	Port     int    `yaml:"port"`
	Username string `yaml:"username"`
	Password string `yaml:"password"`
	Database string `yaml:"database"`
}

type redisConfig struct {
	Host     string `yaml:"host"`
	Port     int    `yaml:"port"`
	Password string `yaml:"password"`
}

type amqpConfig struct {
	Url       string `yaml:"url"`
	Quotation string `yaml:"quotation"`
	KLine     string `yaml:"kline"`
}

func LoadConfig() {
	path, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	configPath := filepath.Join(path)
	viper.AddConfigPath(configPath)
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	if err := viper.ReadInConfig(); err != nil {
		log.Fatal(err)
	}

	conf := &config{}
	err = viper.Unmarshal(conf)
	if err != nil {
		log.Fatalf("Unable to decode into config struct, %v", err)
	}
	Config = conf
}
