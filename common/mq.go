package common

import (
	"context"
	"log"
	"pack.ag/amqp"
	"time"
)

type MQ struct {
	client *amqp.Client
}

type MessageHandler func([]byte) error

var MQHelper MQ

func InitMq() {
	client, err := amqp.Dial(Config.Amqp.Url)
	if err != nil {
		log.Fatal("Dialing AMQP server:", err)
	}
	MQHelper.client = client
}

func (m *MQ) StartConsuming(queue string, handler MessageHandler) {
	session, err := m.client.NewSession()
	if err != nil {
		log.Fatal("Creating AMQP session:", err)
	}

	receiver, err := session.NewReceiver(amqp.LinkSourceAddress(queue), amqp.LinkCredit(10))
	if err != nil {
		log.Fatal("Creating receiver link:", err)
	}

	ctx := context.Background()
	defer func() {
		ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
		receiver.Close(ctx)
		cancel()
	}()

	for {
		// Receive next message
		msg, err := receiver.Receive(ctx)
		if err != nil {
			log.Fatal("Reading message from AMQP:", err)
		}

		// Accept message
		msg.Accept()

		//log.Printf("Message received: %s", msg.GetData())

		if err := handler(msg.GetData()); err != nil {
			log.Printf("Handle message failed(%v), message:%v", err, string(msg.GetData()))
		}
	}
}
