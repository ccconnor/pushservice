package common

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"log"
)

var MysqlHelper *gorm.DB

func InitMysql() {
	url := fmt.Sprintf("%s:%s@(%s:%d)/%s"+`?charset=utf8mb4&parseTime=True&loc=Local`,
		Config.Mysql.Username, Config.Mysql.Password, Config.Mysql.Host, Config.Mysql.Port, Config.Mysql.Database)
	db, err := gorm.Open("mysql", url)
	if err != nil {
		log.Fatalln(err)
	}
	MysqlHelper = db
}
