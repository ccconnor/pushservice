package common

import (
	"fmt"
	"github.com/go-redis/redis"
	"log"
)

var RedisHelper *redis.Client

func InitRedis() {
	client := redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%v:%v", Config.Redis.Host, Config.Redis.Port),
		Password: Config.Redis.Password,
	})

	if err := client.Ping().Err(); err != nil {
		log.Fatalf("Failed to connect redis %v:%v %v", Config.Redis.Host, Config.Redis.Port, err)
	}

	RedisHelper = client
}
