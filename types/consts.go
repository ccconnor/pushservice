package types

const (
	TableQuotation = "quotation"
	TableKLine1m   = "klines_1m"
	TableKLine3m   = "klines_3m"
	TableKLine5m   = "klines_5m"
	TableKLine15m  = "klines_15m"
	TableKLine30m  = "klines_30m"
	TableKLine1h   = "klines_1h"
	TableKLine4h   = "klines_4h"
	TableKLine8h   = "klines_8h"
	TableKLine12h  = "klines_12h"
	TableKLine1d   = "klines_1d"
	TableKLine7d   = "klines_7d"
)

const (
	SymbolBTCUSDT = "BTC-USDT"
)

const (
	KLineType1m  = "1m"
	KLineType3m  = "3m"
	KLineType5m  = "5m"
	KLineType15m = "15m"
	KLineType30m = "30m"
	KLineType1h  = "1h"
	KLineType4h  = "4h"
	KLineType8h  = "8h"
	KLineType12h = "12h"
	KLineType1d  = "1d"
	KLineType7d  = "7d"
)

var supportedTables = map[string]bool{
	TableQuotation: true,
	TableKLine1m:   true,
	TableKLine3m:   true,
	TableKLine5m:   true,
	TableKLine15m:  true,
	TableKLine30m:  true,
	TableKLine1h:   true,
	TableKLine4h:   true,
	TableKLine8h:   true,
	TableKLine12h:  true,
	TableKLine1d:   true,
	TableKLine7d:   true,
}

var supportedSymbols = map[string]bool{
	SymbolBTCUSDT: true,
}

var supportedKLineTypes = map[string]string{
	KLineType1m:  TableKLine1m,
	KLineType3m:  TableKLine3m,
	KLineType5m:  TableKLine5m,
	KLineType15m: TableKLine15m,
	KLineType30m: TableKLine30m,
	KLineType1h:  TableKLine1h,
	KLineType4h:  TableKLine4h,
	KLineType8h:  TableKLine8h,
	KLineType12h: TableKLine12h,
	KLineType1d:  TableKLine1d,
	KLineType7d:  TableKLine7d,
}

func IsTableSupported(table string) bool {
	_, found := supportedTables[table]
	return found
}

func IsSymbolSupported(symbol string) bool {
	_, found := supportedSymbols[symbol]
	return found
}

func GetTableNameByKLineType(klineType string) string {
	table, _ := supportedKLineTypes[klineType]
	return table
}
