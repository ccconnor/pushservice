package types

import "strings"

type Topic struct {
	Table  string
	Symbol string
}

func (t Topic) Encode() string {
	return t.Table + ":" + t.Symbol
}

func (t *Topic) Decode(topic string) {
	items := strings.SplitN(topic, ":", 2)
	t.Table = items[0]
	if len(items) == 2 {
		t.Symbol = items[1]
	}
}

func (t Topic) IsValid() bool {
	return IsSymbolSupported(t.Symbol) && IsTableSupported(t.Table)
}
