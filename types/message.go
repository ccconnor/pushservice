package types

import "pushservice/models"

type Message struct {
	Table string      `json:"table"`
	Data  interface{} `json:"data"`
}

type IMessage interface {
	GetTable() string
	GetSymbol() string
}

type QuotationMessage struct {
	Table string           `json:"table"`
	Data  models.Quotation `json:"data"`
}

func (q QuotationMessage) GetTable() string {
	return q.Table
}

func (q QuotationMessage) GetSymbol() string {
	return q.Data.Symbol
}

type KLineMessage struct {
	Table string        `json:"table"`
	Data  models.KLines `json:"data"`
}

func (k KLineMessage) GetTable() string {
	return k.Table
}

func (k KLineMessage) GetSymbol() string {
	return k.Data.Symbol
}
