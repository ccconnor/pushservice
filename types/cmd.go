package types

const (
	CmdSub          = "Sub"
	CmdUnSub        = "UnSub"
	CmdGetHistKLine = "GetHistKLine"
)

type Command struct {
	Req     string      `json:"req"`
	Rid     string      `json:"rid"`
	Args    interface{} `json:"args"`
	Expires int64       `json:"expires"`
}

type SubOption []string

type UnSubOption []string

type GetHistKLineOption struct {
	Sym   string `json:"Sym"`
	Typ   string `json:"Typ"`
	Sec   int64  `json:"Sec"`
	Count int    `json:"Count"`
}

type Response struct {
	Rid  string      `json:"rid"`
	Code int         `json:"code"`
	Data interface{} `json:"data"`
}
