package types

const (
	StatusOK   = 0
	StatusFail = 1
)

var statusText = map[int]string{
	StatusOK:   "OK",
	StatusFail: "Failed",
}

func StatusText(code int) string {
	return statusText[code]
}
