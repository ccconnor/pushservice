package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
	"pushservice/dao"
	"pushservice/types"
	"sync"
	"time"
)

type Session struct {
	hub            *Hub
	conn           *websocket.Conn
	account        string
	topics         sync.Map
	send           chan []byte
	lastActiveTime time.Time
}

const (
	writeWait     = 10 * time.Second
	sendQueueSize = 256
)

var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func AcceptConnection(h *Hub, w http.ResponseWriter, r *http.Request) {
	// 鉴权
	account, err := authorize(r.Header)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		_, _ = w.Write([]byte(err.Error()))
		return
	}

	// 建立连接
	c, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println("Upgrade failed", err)
		return
	}

	session := newSession(h, c, account)
	h.register <- session

	go session.reader()
	go session.writer()

	session.pushMsgToHub([]byte(`{"info":"Welcome to the Realtime API"}`))
}

func newSession(h *Hub, conn *websocket.Conn, account string) *Session {
	return &Session{
		hub:     h,
		conn:    conn,
		account: account,
		send:    make(chan []byte, sendQueueSize)}
}

func authorize(header http.Header) (string, error) {
	return "", nil
}

func (s *Session) reader() {
	defer func() {
		s.hub.unregister <- s
		s.conn.Close()
	}()
	for {
		_, message, err := s.conn.ReadMessage()
		if err != nil {
			//log.Println("Read failed", err)
			return
		}
		s.onRecvMessage(message)
	}
}

func (s *Session) writer() {
	defer s.conn.Close()
	for {
		message, ok := <-s.send
		if !ok {
			return
		}
		_ = s.conn.SetWriteDeadline(time.Now().Add(writeWait))
		if err := s.conn.WriteMessage(websocket.TextMessage, message); err != nil {
			log.Println("Write failed", err)
			return
		}
	}
}

func (s *Session) onRecvMessage(message []byte) {
	if string(message) == "ping" {
		s.pushMsgToHub([]byte("pong"))
		return
	} else if string(message) == "help" {
		s.pushMsgToHub([]byte("一些帮助信息"))
		return
	}

	var cmd types.Command
	_ = json.Unmarshal(message, &cmd)

	switch cmd.Req {
	case types.CmdSub:
		s.onCmdSub(&cmd)
	case types.CmdUnSub:
		s.onCmdUnSub(&cmd)
	case types.CmdGetHistKLine:
		s.onCmdGetHistKLine(&cmd)
	default:
		log.Println("Unknown cmd", cmd.Req)
	}
}

func (s *Session) onCmdSub(cmd *types.Command) {
	var option types.SubOption
	jsonArgs, _ := json.Marshal(cmd.Args)
	_ = json.Unmarshal(jsonArgs, &option)

	for _, v := range option {
		var topic types.Topic
		topic.Decode(v)
		if !topic.IsValid() {
			log.Printf("Invalid topic %+v", topic)
			continue
		}
		s.topics.LoadOrStore(v, nil)
	}

	s.sendResponse(cmd.Rid, types.StatusOK, types.StatusText(types.StatusOK))
}

func (s *Session) onCmdUnSub(cmd *types.Command) {
	var option types.UnSubOption
	jsonArgs, _ := json.Marshal(cmd.Args)
	_ = json.Unmarshal(jsonArgs, &option)

	for _, v := range option {
		s.topics.Delete(v)
	}

	s.sendResponse(cmd.Rid, types.StatusOK, types.StatusText(types.StatusOK))
}

func (s *Session) onCmdGetHistKLine(cmd *types.Command) {
	var option types.GetHistKLineOption
	jsonArgs, _ := json.Marshal(cmd.Args)
	_ = json.Unmarshal(jsonArgs, &option)

	if !types.IsSymbolSupported(option.Sym) {
		s.sendResponse(cmd.Rid, types.StatusFail, fmt.Sprintf("symbol %s not supported", option.Sym))
		return
	}

	table := types.GetTableNameByKLineType(option.Typ)
	if table == "" {
		s.sendResponse(cmd.Rid, types.StatusFail, fmt.Sprintf("kline %s not supported", option.Typ))
		return
	}

	if option.Count > 1440 {
		option.Count = 1440
	}

	klines, err := dao.KLineDao.GetHistory(option.Sym, table, time.Unix(option.Sec, 0), option.Count)
	if err != nil {
		log.Println("Get kline history failed,", table, err)
		s.sendResponse(cmd.Rid, types.StatusFail, types.StatusText(types.StatusFail))
	} else {
		s.sendResponse(cmd.Rid, types.StatusOK, klines)
	}
}

func (s *Session) sendResponse(rid string, code int, data interface{}) {
	resp := types.Response{
		Rid:  rid,
		Code: code,
		Data: data,
	}
	marshaledResp, _ := json.Marshal(resp)
	s.pushMsgToHub(marshaledResp)
}

// 保证只有hub会向session的send chan发送数据
func (s *Session) pushMsgToHub(message []byte) {
	data := &ResponseWrapper{
		session: s,
		data:    message,
	}
	s.hub.response <- data
}
