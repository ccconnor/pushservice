package main

import (
	"encoding/json"
	"log"
	"pushservice/common"
	"pushservice/types"
	"time"
)

type Hub struct {
	sessions   map[*Session]bool
	push       chan types.IMessage
	response   chan *ResponseWrapper
	register   chan *Session
	unregister chan *Session
}

type ResponseWrapper struct {
	session *Session
	data    []byte
}

const (
	checkInterval = 5 * time.Second
	deadTime      = 30 * time.Second
)

func NewHub() *Hub {
	return &Hub{
		push:       make(chan types.IMessage),
		response:   make(chan *ResponseWrapper),
		register:   make(chan *Session),
		unregister: make(chan *Session),
		sessions:   make(map[*Session]bool),
	}
}

func (h *Hub) Start() {
	go h.run()
	go common.MQHelper.StartConsuming(common.Config.Amqp.Quotation, h.handleQuotationMessage)
	go common.MQHelper.StartConsuming(common.Config.Amqp.KLine, h.handleKLineMessage)
}

// 唯一可以向session的send chan发送数据和关闭send chan的goroutine
func (h *Hub) run() {
	ticker := time.NewTicker(checkInterval)
	defer ticker.Stop()
	for {
		select {
		case session := <-h.register:
			h.registerSession(session)
		case session := <-h.unregister:
			h.unregisterSession(session)
		case response := <-h.response:
			h.pushResponse(response)
		case message := <-h.push:
			h.pushMessage(message)
		case <-ticker.C:
			h.checkAlive()
		}
	}
}

func (h *Hub) handleMessage(message []byte) error {
	return nil
}

func (h *Hub) handleKLineMessage(message []byte) error {
	var msg types.KLineMessage
	_ = json.Unmarshal(message, &msg)
	h.push <- msg

	return nil
}

func (h *Hub) handleQuotationMessage(message []byte) error {
	var msg types.QuotationMessage
	_ = json.Unmarshal(message, &msg)
	h.push <- msg

	return nil
}

func (h *Hub) registerSession(session *Session) {
	h.sessions[session] = true
	log.Println("A new session comes in")
}

func (h *Hub) unregisterSession(session *Session) {
	if _, ok := h.sessions[session]; ok {
		delete(h.sessions, session)
		close(session.send)
		log.Println("A session leaves")
	}
}

func (h *Hub) sendDataToSession(session *Session, message []byte) {
	select {
	case session.send <- message:
		session.lastActiveTime = time.Now()
	default:
		log.Println("Session send queue is full")
		close(session.send)
		delete(h.sessions, session)
	}
}

func (h *Hub) pushResponse(response *ResponseWrapper) {
	if _, ok := h.sessions[response.session]; ok {
		h.sendDataToSession(response.session, response.data)
	}
}

func (h *Hub) pushMessage(message types.IMessage) {
	topic := types.Topic{Table: message.GetTable(), Symbol: message.GetSymbol()}.Encode()
	jsonMsg, _ := json.Marshal(message)
	for session := range h.sessions {
		if _, ok := session.topics.Load(topic); ok {
			h.sendDataToSession(session, jsonMsg)
		}
	}
}

func (h *Hub) checkAlive() {
	var count int
	for session := range h.sessions {
		if session.lastActiveTime.Add(deadTime).Before(time.Now()) {
			log.Println("Session expires")
			close(session.send)
			delete(h.sessions, session)
		} else {
			count++
		}
	}
	log.Println(count, "live sessions")
}
