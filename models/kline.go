package models

import (
	"github.com/shopspring/decimal"
)

type KLines struct {
	Symbol    string          `json:"symbol"`
	Timestamp int64           `json:"timestamp"`
	Open      decimal.Decimal `json:"open"`
	Close     decimal.Decimal `json:"close"`
	High      decimal.Decimal `json:"high"`
	Low       decimal.Decimal `json:"low"`
	Volume    decimal.Decimal `json:"volume"`
	Turnover  decimal.Decimal `json:"turnover"`
}
