package models

import (
	"github.com/shopspring/decimal"
)

type Quotation struct {
	Symbol    string          `json:"symbol"`
	Timestamp int64           `json:"timestamp"`
	Price     decimal.Decimal `json:"price"`
	Volume    decimal.Decimal `json:"volume"`
}
