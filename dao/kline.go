package dao

import (
	"encoding/json"
	"pushservice/common"
	"pushservice/models"
	"time"
)

type kLineDaoClass struct {
}

var KLineDao kLineDaoClass

func (kLineDaoClass) GetHistory(symbol string, table string, beginTime time.Time, count int) ([]models.KLines, error) {
	var klines []models.KLines
	//err := common.MysqlHelper.Table(table).
	//	Where("symbol=? and timestamp>=?", symbol, beginTime).
	//	Limit(count).Find(&klines).Error
	//if err != nil {
	//	return nil, err
	//}

	res, err := common.RedisHelper.Get(table).Result()
	if err != nil {
		return nil, err
	}
	if err := json.Unmarshal([]byte(res), &klines); err != nil {
		return nil, err
	}
	return klines, nil
}
