package main

import (
	"fmt"
	"log"
	"net/http"
	"pushservice/common"
)

func main() {
	common.LoadConfig()
	common.InitRedis()
	common.InitMq()

	hub := NewHub()
	hub.Start()

	http.HandleFunc("/realtime", func(writer http.ResponseWriter, request *http.Request) {
		AcceptConnection(hub, writer, request)
	})

	log.Fatalln(http.ListenAndServe(fmt.Sprintf("%v:%v", common.Config.Addr, common.Config.Port), nil))
}
